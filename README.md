Instalando o e-IQUAS em Linux Debian

Os índices de qualidade de água são números adimensionais usados para apoiar o processo de caracterização da qualidade de água, calculados a partir dos teores de parâmetros físico-químicos presentes em uma amostra. O Índice de Qualidade de Uso da Água Subterrânea (e-IQUAS) por sua vez, é uma formulação diferenciada das demais, pois não utiliza o sistema de pesos e tem como característica principal a flexibilidade na seleção dinâmica dos parâmetros analisados. 
O e-IQUAS foi desenvolvido a partir da filosofia do Software Livre, utilizando as seguintes linguagens de programação, junto com os seus frameworks e/ou bibliotecas:

    - Para o back-end, foi utilizado o PHP 7.2, junto com o Laravel (v5.4); 
    - No front-end foi utilizado o HTML5 e Javascript, através do jQuery; 
    - Na estilização das páginas, utilizou-se CSS3 (W3SCHOOLS, 2019) com Bootstrap 4;
    - Para o armazenamento dos dados, optou-se pelo SGBD (Sistema Gerenciador de Banco de Dados) MySQL.

0. Docker

Você pode executar a aplicação utilizando Docker. Para isso, instale o Docker Compose e execute `docker-compose build` e `docker-compose up` na raiz da aplicação. Ela estará disponível em http://localhost:3000. Neste caso, os passos de 1 a 5 abaixo podem ser pulados.

1. Instalação do PHP7.3

PHP é uma linguagem interpretada livre, usada originalmente apenas para o desenvolvimento de aplicações presentes e atuantes no lado do servidor, capazes de gerar conteúdo dinâmico na World Wide Web.	

O framework que iremos utilizar requer que a versão do PHP seja ao menos a 7.2.5, dessa forma iremos utilizar durante essa instalação a versão 7.3.

Para instalar o PHP7.3 e suas dependências, utilizaremos o gerenciador de pacotes apt, que nos provê as últimas atualizações dos pacotes requeridos através de um canal seguro e confiável.

    $ sudo apt install php7.3 php7.3-zip php7.3-xml php7.3-mbstring php7.3-mysql

Para instalar uma versão acima da PHP7.3, confira no site https://www.php.net/downloads.


2. Instalação do Laravel e Composer

Laravel é um framework PHP livre e open-source criado por Taylor B. Otwell para o desenvolvimento de sistemas web que utilizam o padrão MVC.
Para instalar o Laravel, utilizaremos o gerenciador de pacotes Composer que irá gerenciar as dependências necessárias

2.1 Composer
O Composer é um gerenciador de pacotes no nível do aplicativo para a linguagem de programação PHP que fornece um formato padrão para gerenciar dependências do software PHP e bibliotecas necessárias

Pode encontrar instruções em: https://getcomposer.org/download/
Siga os passos orientados para download e instalação.

Após a instalação bem sucedida, iremos mover o arquivo gerado composer.phar para funcionar de forma global através do comando abaixo:
	
    $ mv composer.phar /usr/local/bin/composer


2.2 Instalação do Laravel
Começaremos por baixar o Laravel installer através do composer

	$ composer global require laravel/installer

Após a instalação do Laravel, iremos executar o composer para baixar as dependências

	$ composer install

Então executamos o comando para atualizar as dependências instaladas para a última versão

    $ composer update


3. Instalação do Banco de Dados Mysql

O MySQL é um sistema de gerenciamento de banco de dados open source, que utiliza a linguagem SQL como interface. Muito utilizado em projetos de variadas dimensões, por possuir boa consistência, alta performance, confiabilidade e ser fácil para usar. O MariaDB é um fork do Mysql, dos mesmo criadores, com o intuito de manter uma licença com maiores liberdades para o usuário.

Utilizaremos os comandos do mysql, porém faremos a instalação do MariaDB devido a compatibilidade com o sistema Debian e as semelhanças entre os dois. 
A versão do MariaDB que usaremos é a 10.3.

Criaremos uma base de dados para armazenar os dados do e-IQUAS.

3.1 Instalação
Instalaremos as versões 10.3 do cliente e servidor do MariaDB que permite a interface de consulta

	$ sudo apt install mariadb-client-10.3 mariadb-server-10.3

3.3 Criação da base de dados
Criaremos um usuário e uma base de dados para a configuração no Laravel
Começaremos entrando no Mysql através do usuário root
	$ mysql -u root -p

Após entrarmos na interface do Mysql, realizaremos a criação da base de dados.
A database será nomeada como BDEIQUAS

	$ CREATE DATABASE BDEIQUAS;

Para verificar que a base de dados foi criada, utilizaremos o comando abaixo que mostrará todas as existentes 

	$ show databases;


3.2 Criação de usuário
Iremos criar um novo usuário para que possa ter privilégios diferentes do usuário root. 
Ao usuário damos o nome de EIQUAS e a senha nomeamos como PSWEIQUAS

	$ CREATE USER EIQUAS@localhost IDENTIFIED BY ‘PSWEIQUAS’;

Daremos as permissões necessárias ao usuário para que ele possa realizar ações na base de dados já criada. Concedemos permissão para todas as tabelas da base BDEIQUAS do usuário EIQUAS

	$ GRANT ALL PRIVILEGES ON BDEIQUAS.* TO EIQUAS@'localhost';

Por fim, faremos o comando que irá recarregar as permissões do banco

	$ FLUSH PRIVILEGES;

Você pode verificar se o usuário agora existe usando o comando para listar usuários

	$ SELECT user FROM mysql.user;

Caso precise deletar um usuário, utilize o comando abaixo que na situação, irá deletar o usuário EIQUAS

	$ DROP user EIQUAS@localhost;

O comando abaixo é utilizado para sair da interface do Mysql

	$ exit


4. Configuração do arquivo .env

Com o Mysql finalizado, voltaremos para a configuração do Laravel
Iremos configurar o arquivo .env que é um arquivo que centraliza as variáveis de configuração personalizadas que são necessárias para o software.

Pode encontrar um exemplo do arquivo no endereço do github do Laravel e baixá-lo para copiá-lo para a pasta do e-iquas
https://github.com/laravel/laravel/blob/master/.env.example
O arquivo .env é um arquivo oculto.

4.1 Arquivo .env
O arquivo .env aberto conterá diversas variáveis, só editaremos algumas.
No início do arquivo, você encontrará as variáveis APP_NAME, APP_ENV, APP_KEY, APP_DEBUG, APP_URL.

Iremos entender cada uma dessas variáveis:

I.  APP_NAME
É o nome da aplicação, esse nome aparecerá na janela correspondente do navegador.

II.  APP_ENV
Informa o tipo de ambiente que estamos executando a aplicação. O aconselhável em produção é definir o valor production. Isso porque o Laravel tem uma série de proteção quando ele está configurado assim.

III.  APP_KEY
Chave encriptografada da aplicação. É responsável por manter a segurança de outras funcionalidades que precisam de criptografia. 

IV.  APP_DEBUG
Indica para o Laravel se ele deve mostrar erros no navegador. É perigoso exibir informações caso não seja em ambiente local, por isso é aconselhável manter como false.

V.  APP_URL
Endereço de URL da aplicação.

Manteremos esses valores de variáveis para funcionar apenas em ambiente local

	APP_NAME=eiquas
	APP_ENV=local
	APP_KEY=
	APP_DEBUG=true
	APP_URL=http://localhost

A chave para a variável APP_KEY será gerada por um comando do Laravel.
O valor http://localhost  da variável APP_URL indica o próprio ambiente local como URL.

Abaixo das variáveis que configuramos, encontraremos as variáveis de configuração do banco de dados. Editaremos as variáveis DB_DATABASE, DB_USERNAME e DB_PASSWORD com os dados que criamos no passo anterior

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE='nome-da-base-de-dados'
    DB_USERNAME='usuário-do-banco'
    DB_PASSWORD='senha-do-usuário'

	DB_DATABASE=EIQUASDB
	DB_USERNAME=EIQUAS
	DB_PASSWORD=PSWEIQUAS


4.2 Configurando o Laravel
Após salvarmos o arquivo .env, iremos realizar as configurações através do comando php artisan

Iremos utilizar o comando abaixo para criar as tabelas do e-QUAS no banco de dados, ele irá utilizar os dados que alteramos no arquivo .env

	$ php artisan migrate

Para gerar a chave encriptografada da variável APP_KEY, utilizaremos o seguinte comando

	$ php artisan key:generate


5. Acessando o e-IQUAS

Após finalizar os procedimentos de instalação e configuração, acessaremos a aplicação através do Laravel pelo comando

	$ php artisan serve

Ele habilita hostear de forma local a aplicação, por padrão vem configurado para rodar em localhost na porta 8000.
Poderá acessar a aplicação através do navegador pelo link http://127.0.0.1:8000


6. Adicionando usuário pré-cadastrado

Para fazer Login como administrador na aplicação enquanto ela estiver rodando, será necessário adicionar um usuário pré-cadastrado.

Entre no arquivo DatabaseSeeder.php dentro das pastas database->seeds
Dentro da função public function run(), adicione as seguintes linhas

	\App\User::insert([
            'name' => 'Admin',
            'password' => Hash::make('admin'),
            'email' => 'administrador@email.com'
        ]);

As informações podem ser alteradas segundo as instruções abaixo

	'name' => 'nome-do-usuario',
            'password' => Hash::make('senha-do-usuario'),
            'email' => 'email-do-usuario'

Para atualizar a aplicação com essa alteração, faça o seguinte comando

	$ php artisan db:seed
